from django.db import models

# Create your models here.
class Schedule(models.Model):
    date = models.DateField(blank=False)
    time = models.TimeField(blank=False)
    activity = models.CharField(max_length=50, blank=False)
    place = models.CharField(max_length=100, blank=False)
    activity_type = models.CharField(max_length=100, blank=False)
 