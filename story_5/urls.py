from django.urls import path
from .views import schedule_show, schedule_remove, schedule_form

urlpatterns = [
   path('', schedule_form, name = 'schedule'),
   path('result/', schedule_show, name = 'scheduleresult'),
   path('result/<int:id>',schedule_remove, name = 'scheduleremove'),
]
