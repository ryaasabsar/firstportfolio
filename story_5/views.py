from django.shortcuts import render, redirect
from .models import Schedule
from .forms import ScheduleForm

# Create your views here.
def schedule_form(request):
    if request.method == "POST":
        form = ScheduleForm(request.POST)
        if form.is_valid():
            print("test")
            schedule = Schedule()
            schedule.activity = form.cleaned_data['activity']
            schedule.date = form.cleaned_data['date']
            schedule.time = form.cleaned_data['time']
            schedule.place = form.cleaned_data['place']
            schedule.activity_type = form.cleaned_data['activity_type']
            schedule.save()
            return redirect('result/')
        return render(request,'story_5/schedule.html',{'form' : form})
    form = ScheduleForm()
    return render(request,'story_5/schedule.html',{'form' : form})


def schedule_show(request):
    schedule = Schedule.objects.all().order_by('time').order_by('date')
    response = {
        'schedule':schedule, 
    }
    return render(request,'story_5/scheduleresult.html',response)

def schedule_remove(request, id):
    Schedule.objects.filter(id=id).delete()
    schedule = Schedule.objects.all().order_by('time').order_by('date')
    response = {
        'schedule':schedule, 
    }
    return render(request, 'story_5/scheduleresult.html',response)

