from django import forms

class ScheduleForm(forms.Form):
    date = forms.DateField(widget=forms.DateInput(attrs={
        'class': 'form-control',
        'placeholder' : 'DD/MM/YYYY',
        'type' : 'date',
        'required': True,
    }))

    time = forms.TimeField(widget=forms.TimeInput(attrs={
        'class': 'form-control',
        'type' : 'time',
        'required': True,
    }))

    activity = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Activity',
        'required': True,
    }))

    place = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Place',
        'required': True,
    }))

    activity_type = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Type',
        'required': True,
    }))
